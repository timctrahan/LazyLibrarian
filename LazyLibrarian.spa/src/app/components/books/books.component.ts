import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { BookMin } from '@app/models/Book.model';
import { BookService } from '@app/services';
import { eBookService } from '@app/services/lib/lazylib.service';
import { BaseGridComponent } from '@shared/components';
import { ToastrService } from 'ngx-toastr';
import { environment as env } from '@env/environment';
import { BooksGridButtonsComponent } from './books-grid-buttons.component';
@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent extends BaseGridComponent<BookMin> implements OnInit {

  public eBookList: BookMin[] = [];
  public context = { componentParent: this };
  private frameworkComponents = {
    buttonsRenderer: BooksGridButtonsComponent
  };
  constructor(
    private ebService: BookService,
    public toastr: ToastrService,
    public ref: ChangeDetectorRef,
    ) {

      super(toastr, ref);
      this.gridOptions.context = this.context;
      this.gridOptions.frameworkComponents = this.frameworkComponents;

      this.gridOptions.columnDefs = [
        {
          // autoHeight: true,
          headerName: 'Cover',
          cellRenderer: params => {
              // put the value in bold
              const ret = `<img src="${env.baseURL}/${params.data.BookImg}" alt="Cover" class="bookcover-sm img-fluid">`;
              return ret;
          },
          suppressAutoSize: true,
          width: 60,
          maxWidth: 100
        },
        {
          field: 'AuthorName',
          headerName: 'Author Name',
          maxWidth: 150
        },
        {
          field: 'BookName',
          headerName: 'Book Name',
          maxWidth: 225,
          wrapText: true
          //autoHeight: true
        },
        {
          field: 'series'
        },
        {
          cellRenderer: 'buttonsRenderer',
          suppressAutoSize: true,
          maxWidth:100
        }
      ];
  };
  test = () => {
    alert('works');
  }
  ngOnInit(): void {
    this.ebService.getAll().subscribe(eBooks => {
      // this.eBookList = eBooks.map(model => {
      //   const typedModel = this.bookInstance.fromObject(model) as Book;
      //   return typedModel;
      // });
      this.gridOptions?.api?.setRowData(eBooks);
    });
  }

}
