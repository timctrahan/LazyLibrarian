import { Component } from '@angular/core';
import * as _ from 'lodash';
import { BaseGridButtonsComponent, BaseComponent } from '@shared/components';
import { Book } from '@app/models/Book.model';
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
@Component({
  selector: 'app-books-grid-buttons',
  template: `
  <div class="text-center">

    <div *ngIf='model.Status==="Open"'>
      <button style='margin-top: 5px; width:80px;' class="btn btn-primary btn-sm" type="button" title="Send" (click)="onSendBookClick($event)">
        <span><fa-icon [icon]="['fas', 'envelope']" size="lg"></fa-icon></span>&nbsp;&nbsp;Send</button>
      <br/>
      <button style='margin-top:5px; width:80px;' class="btn btn-warning btn-sm" type="button" title="Open" (click)="onSendBookClick($event)">
        <span><fa-icon [icon]="['fas', 'book-open']" size="lg"></fa-icon></span>&nbsp;&nbsp;Open</button>
    </div>

    <div *ngIf='model.Status==="Wanted"'>
      <button style='margin-top:5px; width:80px;' class="btn btn-danger btn-sm" type="button" title="Wanted" (click)="onSendBookClick($event)">
        <span><fa-icon [icon]="['fas', 'cart-arrow-down']" size="lg"></fa-icon></span>&nbsp;&nbsp;Wanted</button>
        <br/>
      <button style='margin-top:5px; width:80px;' class="btn btn-success btn-sm" type="button" title="Search" (click)="onSendBookClick($event)">
        <span><fa-icon [icon]="['fas', 'eye']" size="lg"></fa-icon></span>&nbsp;&nbsp;Search</button>
      </div>
      <button *ngIf='model.Status==="Skipped"' style='margin-top:5px; width:80px;' class="btn btn-secondary btn-sm" type="button" title="Skipped" (click)="onSendBookClick($event)">
        <span><fa-icon [icon]="['fas', 'book']" size="lg"></fa-icon></span>&nbsp;&nbsp;Skipped</button>

    </div>
  `
})
export class BooksGridButtonsComponent extends BaseGridButtonsComponent<Book, BaseComponent<Book>> {
  constructor() {
      super();
    }

    onSendBookClick = ($event: MouseEvent) => {
      console.log(this.model.getAllCmd);
    }
}

