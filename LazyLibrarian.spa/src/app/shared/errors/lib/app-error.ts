export class AppError {
  constructor(public originalError?: any, public message?: string) { }
}
