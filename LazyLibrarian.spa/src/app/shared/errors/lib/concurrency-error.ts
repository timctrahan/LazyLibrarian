import { AppError } from '@app/shared/errors/lib/app-error';

export class ConcurrencyError extends AppError {}
