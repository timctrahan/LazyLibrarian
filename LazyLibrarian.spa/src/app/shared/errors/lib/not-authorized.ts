import { AppError } from "@app/shared/errors/lib/app-error";

export class NotAuthorized extends AppError{}
