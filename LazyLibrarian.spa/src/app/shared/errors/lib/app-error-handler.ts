import { ErrorHandler, Injectable } from '@angular/core';
import { BadInput } from './bad-input';
import { NotAuthorized } from './not-authorized';
import { NotFoundError } from './not-found-error';
import { ServerUnavailable } from './server-unavailable';

@Injectable()
// Global Error Handler to handle all unexpected errors
export class AppErrorHandler implements ErrorHandler {

  constructor() { }

  handleError(error: any) {
    let message = '';

    if (error instanceof NotFoundError) {
      message = 'Not Found Response Returned';
    } else {
      if (error instanceof NotAuthorized) {
        message = 'Not Authorized Response Returned';
      } else {
        if (error instanceof BadInput) {
          message = 'Bad Input Response Returned';
        } else {
          if (error instanceof ServerUnavailable) {
            message = 'Application Server Unavailable';
          } else {
            if (error.originalError) {
              message = error.originalError.message;
            } else {
              if (error.message) {
                message = error.message;
              } else {
                message = 'Unexpected Error Occurred';
              }
            }
          }
        }
      }
    }


    // this.notificationService.smartMessageBox({
    //     title: '<i class=\'fa fa-warning\' style=\'color:yellow\'></i> Unexpected Error',
    //     content: message + '...(Global Handler)',
    //     buttons: '[OK]'
    //   });
  }
}
