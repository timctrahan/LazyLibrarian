import { AppError } from "@app/shared/errors/lib/app-error";

export class NotFoundError extends AppError{}
