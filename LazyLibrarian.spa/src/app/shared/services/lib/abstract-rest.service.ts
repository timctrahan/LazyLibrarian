/* eslint-disable max-len */
import { Observable, throwError, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment as env } from '@env/environment';
import { BaseModel, IBaseModel, ModelDataChange } from '@shared/models';
import { BaseDataService } from './base-data.service';
import { Dictionary, forEach } from 'lodash';

type CmdTypes = 'getAll' | 'getOne' | 'create' | 'update' | 'delete';

export abstract class AbstractRestService<T extends BaseModel> extends BaseDataService {

  //#region Private Variables
  public modelInstance: T;
  private _typeName: string;
  private _actionUrl: string;
  private _byKey!: Dictionary<T>;
  private _modelData: T[] = [];


  //#endregion

  //#region getters/setters
  public get modelData(): T[] {
    return this._modelData;
  }
  public set modelData(newModelData: T[]) {
    // handle sorting function
    this._modelData = newModelData.map(model => {
      const typedModel = this.modelInstance.fromObject(model) as T;
      return typedModel;
    });
  }
  // public get actionUrl(): string {
  //   return this._actionUrl;
  // }
  public get modelByKey(): Dictionary<T> {
    return this._byKey;
  }
  //#endregion

  constructor(
    /** The class for this service. Should be the same as T */
    public modelType: (new () => T),
    protected httpClient: HttpClient,
    /** Optional action url to use for the http endpoint. Defaults to the environment webApiURL/typename */
    actionUrl?: string) {
    super();

    this.modelInstance = new modelType();
    this._typeName = this.modelInstance.typeName.toLowerCase();
    this._actionUrl = (actionUrl ? actionUrl : `${env.webApiURL}/${this._typeName}`);
  }

  getActionUrl = (cmdType: CmdTypes): string => {
    switch (cmdType) {
      case 'getAll':
        return `${env.webApiURL}&cmd=${this.modelInstance.getAllCmd}`;
      case 'getOne':
        return `${env.webApiURL}&cmd=${this.modelInstance.getOneCmd}`;
      case 'create':
        return `${env.webApiURL}&cmd=${this.modelInstance.createCmd}`;
      case 'update':
        return `${env.webApiURL}&cmd=${this.modelInstance.updateCmd}`;
      case 'delete':
        return `${env.webApiURL}&cmd=${this.modelInstance.deleteCmd}`;
    }
  }

  getAll = (forceRefresh = false, hideLoader = false): Observable<T[]> => {
    if (!(this.modelData?.length > 0) || forceRefresh) {
      let modelData: Observable<T[]>;
      if (hideLoader) {
        modelData = this.httpClient.get<T[]>(this.getActionUrl('getAll'), {headers: { hideLoader : 'true' }});
      } else {
        modelData = this.httpClient.get<T[]>(this.getActionUrl('getAll'));
      }

      return  modelData.pipe(
        map(_modelData => {
          return _modelData.map(model => {
            return this.modelInstance.fromObject(model) as T;
          })
        }),
        tap(_modelData => {
          this.modelData = _modelData;
        })
      );
    }
    return from([this.modelData]);
  };

  getOne = (modelId: string, useCache = true): Observable<T> => {
    if (useCache) {
      return from([this.modelByKey[modelId]]);
    }
    return this.httpClient.get<T>(`${this.getActionUrl('getOne')}&id=${modelId}`)
      .pipe(
        map((_modelData: T) => this.modelInstance.fromObject(_modelData) as T),
        tap((_modelData: T) => {
          this.modelData = this.modelData.map(model => {
            if (model.modelId !== _modelData.modelId) {
              return model;
            }
            return _modelData;
          });
        }),
        catchError(e => this.handleError(e))
      );
  };

  create = (modelData: T): Observable<T> => this.httpClient.post<T>(`${this.getActionUrl('create')}`, modelData)
      .pipe(
        map(_modelData => this.modelInstance.fromObject(_modelData) as T),
        catchError(e => this.handleError(e))
      );

  // update = (modelId: string, modelData: T): Observable<T> => this.httpClient.put<T>(`${this.actionUrl}/${this.splitId(modelId)}`, modelData)
  //     .pipe(
  //       map((_modelData) => this.modelInstance.fromObject(_modelData) as T),
  //       catchError(e => this.handleError(e))
  //     );

  delete = (modelId: string) => this.httpClient.delete<T>(`${this.getActionUrl('getOne')}&id=${modelId}`)
      .pipe(
        catchError(e => this.handleError(e))
      );

  private splitId = (modelId: string): string => {
    const parts = modelId.split(env.modelIdSplitChar);
    switch (parts.length) {
      case 1:
        return `${encodeURIComponent(parts[0])}`;
      case 2:
        return `${encodeURIComponent(parts[0])}/${encodeURIComponent(parts[1])}`;
      case 3:
        return `${encodeURIComponent(parts[0])}/${encodeURIComponent(parts[1])}/${encodeURIComponent(parts[2])}`;
      case 4:
        return `${encodeURIComponent(parts[0])}/${encodeURIComponent(parts[1])}/${encodeURIComponent(parts[2])}/${encodeURIComponent(parts[3])}`;
      default:
        return modelId;

    }
  };
}
