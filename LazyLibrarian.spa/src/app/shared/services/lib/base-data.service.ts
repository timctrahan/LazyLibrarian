import { Injectable } from '@angular/core';
import { environment as env } from '@env/environment';
import { throwError } from 'rxjs';
import { AppError, AppErrorHandler, BadInput, NotAuthorized, NotFoundError, ServerUnavailable } from '@app/shared/errors';
import { ConcurrencyError } from '@app/shared/errors/lib/concurrency-error';

@Injectable()
export abstract class BaseDataService {

  constructor() { }

  public handleError(error: Response) {
    if (error.status === 400) {
      return throwError(new BadInput(error, 'The was an error in the data submitted to the server.'));
    }

    if (error.status === 401 || error.status === 403) {
      return throwError(new NotAuthorized(error, 'You are not authorized to do this task.'));
    }

    if (error.status === 404) {
      return throwError(new NotFoundError(error, 'End point not found.'));
    }

    if (error.status === 409) {
      return throwError(new ConcurrencyError(error, 'This record was modified since loading. Reloading now... Try again.'));
    }

    return throwError(new AppError(error, error.statusText));
  }

}
