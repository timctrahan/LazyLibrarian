/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { PropertyDef } from '@shared/models';
import { Dictionary } from 'lodash';
import 'reflect-metadata';

export const PROPERTY_DEFS = 'property-defs';

export function PropertyDefDecorator(propertyDef?: Partial<PropertyDef>) {
  propertyDef = propertyDef || {};

  return (target: any, propertyKey: string) => {
    // tryinng to keep what's stored in metadata immutable.  Not sure if this is the best way
    const propertyDefs: Dictionary<Partial<PropertyDef>> = Reflect.getMetadata(PROPERTY_DEFS, target) || {};
    propertyDefs[propertyKey] = Object.assign(propertyDef, { propertyKey });
    Reflect.defineMetadata(PROPERTY_DEFS, propertyDefs, target);
  };
}
