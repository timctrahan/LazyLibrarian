/* eslint-disable prefer-arrow/prefer-arrow-functions */
import { ColDef } from 'ag-grid-community';
import 'reflect-metadata';

export const GRID_COL_DEFS = 'grid-col-defs';

export function GridColumnDefDecorator(colDef?: ColDef) {
  colDef = colDef || {};

  return (target: any, propertyKey: string) => {
    // Pull the existing metadata or create an empty object
    let colDefs: ColDef[] = Reflect.getMetadata(GRID_COL_DEFS, target) || [];

    // Auto assign the field property
    colDefs = [...colDefs, Object.assign(colDef, { field: propertyKey })];
    // Update the metadata
    Reflect.defineMetadata(GRID_COL_DEFS, colDefs, target);
  };
}
