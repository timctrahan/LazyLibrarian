import {
  Component,
  OnInit,
  OnDestroy,
  ChangeDetectorRef,
  HostListener,
  AfterViewInit,
  ViewChild,
} from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import {
  GridReadyEvent,
  GridOptions,
  ModelUpdatedEvent,
  FilterChangedEvent,
  ColDef,
  ColumnApi,
} from 'ag-grid-community';

import { BaseModel, EnumChangeType, IBaseModel } from '@shared/models';

import * as _ from 'lodash';

import { BaseComponent } from './base.component';
import { AbstractRestService } from '@shared/services';
import { GRID_COL_DEFS } from '@shared/decorators';
import { AgGridAngular } from 'ag-grid-angular';


@Component({
  selector: 'app-base-grid',
  template: '',
})
export abstract class BaseGridComponent<T extends IBaseModel>
  extends BaseComponent<T>
  implements OnDestroy, OnInit, AfterViewInit {
  @ViewChild(AgGridAngular, { static: false }) agGrid: any;
  public filterText: string | undefined;

  // public get gridColDef(): ColDef[] {
  //   const ret = this.typeMetaData[GRID_COL_DEFS];
  //   return ret;
  // }
  public doGridResizeToFits = true;
  public gridColumnApi!: ColumnApi;
  /**
   * This section configures the grid.
   * These are the default settings and
   */
  public gridOptions: GridOptions = {
    defaultColDef: {
      resizable: true,
      sortable: true,
      filter: true,
      filterParams: {
        newRowsAction: 'keep',
      },
    },
    rowSelection: 'single',
    overlayLoadingTemplate:
      '<span class="ag-overlay-loading-center">Please wait while data is loading...</span>',
    overlayNoRowsTemplate: `
      <span style="padding: 10px; border: 2px solid #444; background: lightgoldenrodyellow;">No data available...</span>`,
    onModelUpdated: ($event: ModelUpdatedEvent) => {
      if (this.doGridResizeToFits) {
        if ($event.newData) {
          setTimeout(() => {
            $event.api.sizeColumnsToFit();
          }, 50);
        }
      }
    },
    // getRowStyle: (params: any) => {
    //   if (params.data.locked) {
    //     if (
    //       this.service.signalRWrapper.connectionIds[
    //       params.data.locked.lockedByConnectionId
    //       ]
    //     ) {
    //       return { background: 'lightgreen' };
    //     } else {
    //       return { background: 'lightyellow' };
    //     }
    //   } else {
    //     return { background: '' };
    //   }
    // },
    getRowNodeId: (data: T) => data.modelId,
    onGridReady: (params: GridReadyEvent) => {
      this.gridColumnApi = params.columnApi;
      const isDeletedFilter = params.api.getFilterInstance('isDeleted');
      if (isDeletedFilter) {
        isDeletedFilter.setModel({
          filterType: 'text',
          type: 'equals',
          filter: 'false',
        });
      }
    },
    onFilterChanged: (params: FilterChangedEvent) => {
      if (this.doGridResizeToFits) {
        params.api.sizeColumnsToFit();
      }
    },
  };

  private _modelInstance!: T;
  constructor(
    // public service: AbstractRestService<T>,
    // public modalService: BsModalService,
    public toastr: ToastrService,
    ref: ChangeDetectorRef
  ) {
    super(toastr, ref);
  }

  @HostListener('window:resize', ['$event']) onResize() {
    if (this.doGridResizeToFits) {
      this.gridOptions?.api?.sizeColumnsToFit();
    }
  }

  // DATA FORMATTING
  currencyFormatter = (value: number, sign: string): string => {
    const sansDec = value?.toFixed(2);
    const formatted = sansDec?.replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    return sign + `${formatted}`;
  };

  // DATA FORMATTING
  dateFormatter = (value: Date) => {
    const dateAsString = value.toString().substring(0, 10);
    const dateParts = dateAsString.split('-');
    return `${dateParts[1]}/${dateParts[2]}/${dateParts[0]}`;
  };

  ngOnInit(): void {
    // this._baseGridComponent_OnInit().subscribe();
  }

  // _baseGridComponent_OnInit(): Observable<T[]> {
  //   // return this._baseComponent_OnInit();
  // }

  ngOnDestroy(): void {
    this._baseGridComponent_OnDestroy();
  }

  ngAfterViewInit(): void {
    if (this.agGrid?.rowData) {
      console.warn(`A row data source was defined on the grid html tag.
      This will cause the grid to reset grid to the top row when data changes. Make sure this is the desired effect`);
    }
  }

  _baseGridComponent_OnDestroy(): void {
    if (this.changesSubscription) {
      this.changesSubscription.unsubscribe();
    }
    this._baseComponent_OnDestroy();
  }

  // subscribeGridChanges = (gridOptions: GridOptions): Subscription => {
  //   this.changesSubscription = this.service.signalRWrapper.subscribeToChanges((change) => {
  //     if ((change.changeType as EnumChangeType) === EnumChangeType.refresh) {
  //       return;
  //     }
  //     const changedModel: T = change.modelData[0];
  //     const modelId = changedModel.modelId;
  //     const rowNode = gridOptions!.api!.getRowNode(modelId);
  //     const rowData: T = rowNode?.data;
  //     const newModel: T = this.service.modelInstance.fromObject({ ...rowData, ...changedModel }) as T;

  //     // const b = Object.assign(rowData, changedModel) as T;
  //     switch (change.changeType as EnumChangeType) {
  //       case EnumChangeType.create:
  //         gridOptions.api.applyTransaction({ add: change.modelData });
  //         break;
  //       case EnumChangeType.locked:
  //       case EnumChangeType.unlocked:
  //         rowData.locked = changedModel.locked;
  //         rowNode?.setData(rowData);
  //         gridOptions.api.flashCells({
  //           rowNodes: [rowNode],
  //         });
  //         break;
  //       case EnumChangeType.update:
  //         rowNode?.setData(newModel);
  //         gridOptions.api.flashCells({
  //           rowNodes: [rowNode],
  //         });
  //         break;

  //       case EnumChangeType.delete:
  //         if (rowNode) {
  //           gridOptions.api.applyTransaction({ remove: [rowNode.data] });
  //         }
  //         break;
  //     }
  //   }
  //   );
  //   return this.changesSubscription;
  // };

  onClearQuickFilter() {
    this._onClearQuickFilter();
  }

  _onClearQuickFilter() {
    this.filterText = '';
    this.onQuickFilterChanged(this.filterText);
  }

  onQuickFilterChanged(filter: string) {
    this.gridOptions?.api?.setQuickFilter(filter);
  }


  modelFactory(type: new() => T, model: T) {
    const b = new type();
    return b.fromObject(model);

  }
}


