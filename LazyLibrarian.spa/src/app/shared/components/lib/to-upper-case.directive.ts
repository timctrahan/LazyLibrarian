import { Directive, ElementRef, Input, HostListener, NgModule, OnChanges, SimpleChanges } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[appToUpperCase]',
})
export class ToUpperCaseDirective implements OnChanges {
  inputElement: HTMLInputElement;
  private navigationKeys = [
    'Backspace',
    'Delete',
    'Tab',
    'Escape',
    'Enter',
    'Home',
    'End',
    'ArrowLeft',
    'ArrowRight',
    'Clear',
    'Copy',
    'Paste',
  ];


  constructor(public el: ElementRef, private readonly control: NgControl) {
    this.inputElement = el.nativeElement;
  }
  @HostListener('input', ['$event.target'])
  public onInput(input: HTMLInputElement): void {
    const caretPos = input.selectionStart;
    this.control!.control!.setValue(input.value.toUpperCase());
    input.setSelectionRange(caretPos, caretPos);
  }

  // @HostListener('keydown', ['$event']) onKeyDown(e: KeyboardEvent): any {
  //   if (
  //     this.navigationKeys.indexOf(e.key) > -1 || // Allow: navigation keys: backspace, delete, arrows etc.
  //     ((e.key === 'a' || e.code === 'KeyA') && e.ctrlKey === true) || // Allow: Ctrl+A
  //     ((e.key === 'c' || e.code === 'KeyC') && e.ctrlKey === true) || // Allow: Ctrl+C
  //     ((e.key === 'v' || e.code === 'KeyV') && e.ctrlKey === true) || // Allow: Ctrl+V
  //     ((e.key === 'x' || e.code === 'KeyX') && e.ctrlKey === true) || // Allow: Ctrl+X
  //     ((e.key === 'a' || e.code === 'KeyA') && e.metaKey === true) || // Allow: Cmd+A (Mac)
  //     ((e.key === 'c' || e.code === 'KeyC') && e.metaKey === true) || // Allow: Cmd+C (Mac)
  //     ((e.key === 'v' || e.code === 'KeyV') && e.metaKey === true) || // Allow: Cmd+V (Mac)
  //     ((e.key === 'x' || e.code === 'KeyX') && e.metaKey === true) // Allow: Cmd+X (Mac)
  //   ) {
  //     // let it happen, don't do anything
  //     return;
  //   }

  //   if (e.keyCode > 32 && e.keyCode < 128) {
  //     // eslint-disable-next-line @typescript-eslint/dot-notation
  //     e.target['value'] += e.key.toUpperCase();
  //     e.preventDefault(); //stop propagation
  //     //must create a "input" event, if not, there are no change in your value
  //     const evt = document.createEvent('HTMLEvents');
  //     evt.initEvent('input', false, true);
  //     e.target.dispatchEvent(evt);
  //   }
  // }

  ngOnChanges(changes: SimpleChanges): void {

  }

}
