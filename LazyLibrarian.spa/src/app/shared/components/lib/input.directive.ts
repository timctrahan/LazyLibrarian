import {
  AfterViewInit,
  Directive,
  ElementRef,
  Input,
  OnInit,
  Renderer2,
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { PropertyDef } from '@shared/models';
import { Dictionary } from 'lodash';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: `input[appInput], select[appInput]`,
})
export class FHInputDirective implements OnInit, AfterViewInit {
  @Input() inputType = 'text';
  @Input() propertyKey?: string;
  @Input() allPropertyDefs: Dictionary<PropertyDef>;
  @Input() labelClass?: string;
  @Input() labelStyle?: string;
  @Input() inputClass?: string;
  @Input() maxLength?: string;
  @Input() readonly?: string;
  @Input() tooltip?: string;
  @Input() colSize?: number;
  // @Input() required?: boolean;

  private propertyDefs: PropertyDef;
  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
    private control: NgControl
  ) {

  }

  public markAsDirty = () => {
    this.control!.control!.markAsDirty();
  };

  public focus = () => {
    this.el.nativeElement.focus();
  };
  ngAfterViewInit(): void {

  }

  public get valid(): boolean {
    return this.control.valid || false;
  }
  public get key(): string {

    const cName: string = this.control.name?.toString() || '';
    return this.propertyDefs.propertyKey || cName;
  }
  ngOnInit(): void {
    let readonly: boolean = false;
    if (this.readonly === 'true') {
      readonly = true;
    }
    this.propertyDefs = this.allPropertyDefs[this.key];

    const parentElement = this.el.nativeElement.parentElement;
    // get the number of inputs in this div to calculate the col size
    const inputs = parentElement.querySelectorAll('input, select');
    const colSize = this.colSize || (12 - (2 * inputs.length)) / inputs.length;


    const label = this.renderer.createElement('label');
    const text = this.renderer.createText(`${this.propertyDefs.label}:`);
    this.renderer.appendChild(label, text);
    this.renderer.setAttribute(label, 'class', this.labelClass || 'col-sm-2 col-form-label col-form-label-sm font-weight-bold');
    this.renderer.setAttribute(label, 'style', this.labelStyle || 'text-align: right');

    this.renderer.insertBefore(parentElement, label, this.el.nativeElement);

    this.renderer.setAttribute(this.el.nativeElement, 'class', this.inputClass || `col-sm-${colSize} form-control form-control-sm`);
    this.renderer.setProperty(this.el.nativeElement, 'maxLength', this.propertyDefs.maxLength?.toString());

    if (readonly) {
      this.renderer.setProperty(this.el.nativeElement, 'disabled', true);
      this.renderer.setAttribute(this.el.nativeElement, 'class',
        this.inputClass || `col-sm-${colSize} form-control form-control-sm read-only`);
    }
    if (this.tooltip || this.propertyDefs.toolTip) {
      this.renderer.setAttribute(this.el.nativeElement, 'title', this.tooltip || this.propertyDefs.toolTip || '');
    }
  }

}
