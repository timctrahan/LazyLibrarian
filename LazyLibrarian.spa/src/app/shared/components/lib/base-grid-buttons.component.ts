import { Component } from '@angular/core';
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { GridApi } from 'ag-grid-community';
import { IBaseModel } from '@shared/models';
import { BaseComponent } from '@shared/components';
import * as moment from 'moment';
import * as _ from 'lodash';

@Component({
  selector: 'app-base-grid-component',
  template: ''
})
export abstract class BaseGridButtonsComponent<T extends IBaseModel, C extends BaseComponent<T>> implements ICellRendererAngularComp  {
  public modelId!: string;
  public get model(): T {
    const rowNode = this.gridApi.getRowNode(this.modelId);
    return rowNode!.data;
  }
  public params: any;
  public parentComponent!: C;
  public gridApi!: GridApi;

  agInit(params: any): void {
    this.modelId = params.data.modelId;
    this.parentComponent = params.context.componentParent;
    this.params = params;
    this.gridApi = params.api;
  }

  onForceModelLockedClick = ($event: MouseEvent) => {
    if (this.model.locked) {
      const now = moment();
      const expires = moment(this.model.locked.timeLocked);
      const diff = now.diff(expires);
      const secondsLeft = 300 - moment.duration(diff).as('seconds');
      // this.parentComponent.signalrWrapper.activeConnection.hubConnection.invoke(
      //   'NotifyUser',
      //   this.model.locked.lockedByConnectionId,
      //   'Another user wishes to edit a record you have locked!'
      // );
      this.parentComponent.toastr.info('Sent a message to the user requesting the record be unlocked.',
      'Request Record Unlock', { timeOut: secondsLeft * 1000, progressBar: true, progressAnimation: 'decreasing' });
    }
  };

  onModelEditClick = ($event: MouseEvent): void => {
    // get the rowNode from the grid as the row model data could have changed since the
    // grid buttons control was originally rendered.
    const rowNode = this.gridApi.getRowNode(this.model.modelId);
    // this.parentComponent.onModelEdit(rowNode!.data);
    $event.stopPropagation();
  };

  onModelViewClick = ($event: MouseEvent): void => {
    const rowNode = this.gridApi.getRowNode(this.model.modelId);
    // this.parentComponent.onModelView(rowNode!.data);
    $event.stopPropagation();
  };

  refresh(): boolean {
    return false;
  }
}

