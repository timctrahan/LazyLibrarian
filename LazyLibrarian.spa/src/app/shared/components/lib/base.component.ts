import { Component, OnInit, OnDestroy, ChangeDetectorRef, Inject } from '@angular/core';
import { AbstractRestService } from '@app/shared/services';
import { Subscription, Observable, from } from 'rxjs';
import { IBaseModel, ModelDataChange, PropertyDef } from '@shared/models';
import { AppError } from '@shared/errors/lib/app-error';
import { BsModalService } from 'ngx-bootstrap/modal';
import { ToastrService } from 'ngx-toastr';
import { environment as env } from '@env/environment';

import * as _ from 'lodash';
import { Dictionary } from 'lodash';

import { PROPERTY_DEFS } from '@shared/decorators';
import { HttpErrorResponse } from '@angular/common/http';
import { UntilDestroy } from '@ngneat/until-destroy';
// import { AppInitService } from '@app/app-init.service';


@UntilDestroy({ checkProperties: true })
@Component({
  selector: 'app-base',
  template: ''
})
export abstract class BaseComponent<T extends IBaseModel> implements OnDestroy, OnInit {
  public changesSubscription: Subscription;

  public editModalFlags = {
    showAddEdit: false,
    viewMode: false,
    hide: false
  };

  // public propertyDefs: Dictionary<PropertyDef> = this.typeMetaData[PROPERTY_DEFS];

  /** Use this assessor to get the embedded metadata from the generic model */
  // public get typeMetaData(): Dictionary<any> {
  //   // const emptyModel: T = new this.service.modelType();
  //   const mKeys: string[] = Reflect.getMetadataKeys(emptyModel);
  //   const ret = _.fromPairs(mKeys.map(mKey => [mKey, Reflect.getMetadata(mKey, emptyModel)]));
  //   return ret;
  // }

  // public get modelData(): T[] {
  //   return this.service.modelData;
  // }

  public selectedModel: T;

  constructor(
    // public service: AbstractRestService<T>,
    // public modalService: BsModalService,
    public toastr: ToastrService,
    public ref: ChangeDetectorRef
  ) {
    // super();
  }

  ngOnInit(): void {
    // this._baseComponent_OnInit().subscribe();
  }

  // _baseComponent_OnInit(): Observable<T[]> {
  //   // if (!(this.modelData?.length > 0)) {
  //   //   return this.service.getAll();
  //   // }
  //   return from([this.modelData]);
  // }

  ngOnDestroy(): void {
    this._baseComponent_OnDestroy();
  }

  _baseComponent_OnDestroy(): void {
    if (this.changesSubscription) {
      this.changesSubscription.unsubscribe();
    }
  }

  showToast = (message: string, action = '') => {
    this.toastr.success(message, action);
  };

  handleError = (error: AppError, model?: T) => {
    this._handleError(error, model);
  };

  _handleError = (error: AppError, model?: T) => {
    const res = error.originalError as HttpErrorResponse;
    const errors = res.error?.errors;
    // let msg = error.message;
    switch (res.status) {
      case 400:
        if(errors) {
          _.forIn(errors, ((value, key) => {
            _.forEach(value, errorMessage => {
              this.toastr.error(errorMessage, key, { timeOut: 10000 });
            });
          }));
        } else {
          this.toastr.error(res.error, 'Error', { timeOut: 10000 });
        }
        break;
      case 409: // Model Conflict
            // reload this record
            // this.service.getOne(model.modelId).subscribe((cachedModel) => {
            //   if (model.rowVersion !== cachedModel.rowVersion) {
            //     Object.assign(model, cachedModel);
            //   } else {
            //     // somehow our copy of this object is out of date. pull a fresh copy from the server
            //     this.service.getOne(model.modelId).subscribe();
            //   }
            // });
            this.toastr.error(error.message, 'Error', { timeOut: 10000 });
        break;
      default:
        this.toastr.error(error.message, 'Error', { timeOut: 10000 });
        break;
    }
  };

  // onCloseModal = (modalId: number) => {
  //   this.modalService.hide(modalId);
  // };

  // onModelAdd = (): void => {
  //   this.selectedModel = new this.service.modelType() as T;
  //   this.setShowAddEditValues(true, false);
  // };

  // onModelEdit = (model: T): void => { // gets called from recipient-list-grid-buttons
  //   this.selectedModel = model;
  //   this.setShowAddEditValues(true, false);
  // };

  // onModelView = (model: T): void => { // gets called from recipient-list-grid-buttons
  //   this.selectedModel = model;
  //   this.setShowAddEditValues(true, true);
  // };

  // setShowAddEditValues = (showAddEdit: boolean, viewMode: boolean, hide?: boolean) => {
  //   this.editModalFlags = { // trying to make this immutable
  //     showAddEdit,
  //     viewMode,
  //     hide: (hide) ? hide : false
  //   };

  //   // explcitly detect changes because angular takes its time to figure out the change has been made
  //   this.ref.detectChanges();
  // };
}
