export class RecordLock {
  public modelName!: string;
  public modelId!: string;
  public lockedByConnectionId!: string;
  public timeLocked!: Date;
  public lockGuid!: string;
}
