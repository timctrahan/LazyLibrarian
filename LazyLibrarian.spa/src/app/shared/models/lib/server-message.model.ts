export class ServerMessage {
  senderConnectionId!: string;
  message!: string;
  title!: string;
  toastType!: string;
  timeOut!: number;
}
