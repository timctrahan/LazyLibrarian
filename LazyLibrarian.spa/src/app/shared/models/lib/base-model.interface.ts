import { RecordLock } from '@shared/models';
import { BaseModel } from './base-model.model';

/** The properties in this interface will be available in generic instances */
export interface IBaseModel {
  readonly modelId: string;
  typeName: string;
  rowVersion?: string;
  lastUpdated?: Date;
  locked?: RecordLock;
  isNewRecord?: boolean;

  //api cmd names
  getAllCmd: string;
  getOneCmd: string;
  createCmd: string;
  updateCmd: string;
  deleteCmd: string;

  // fromObject2<T>(o: T): T
  // createNew<T extends BaseModel>(type: (new () => T)): T
  fromObject(o: IBaseModel): IBaseModel;
}

