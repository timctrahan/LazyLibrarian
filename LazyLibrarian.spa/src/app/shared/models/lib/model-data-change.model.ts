import { EnumChangeType } from '@shared/models';

export class ModelDataChange<T> {
  constructor(public originatorId: string, public changeType: EnumChangeType, public modelData: T[]) {}

  static getNew<R>(originatorId: string, changeType: EnumChangeType, modelData: R[]) {
    return new this<R>(originatorId, changeType, modelData);
  }
}
