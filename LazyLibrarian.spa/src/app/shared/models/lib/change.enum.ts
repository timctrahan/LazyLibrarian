export enum EnumChangeType {
  create = 1,
  update = 2,
  delete = 3,
  refresh = 4,
  locked = 5,
  unlocked = 6
}
