import { GridColumnDefDecorator } from '@shared/decorators';
import { RecordLock } from '@shared/models';
import { IBaseModel } from './base-model.interface';

export abstract class BaseModel implements IBaseModel {

  rowVersion?: string;
  lastUpdated?: Date;
  locked?: RecordLock;
  isNewRecord = false;
  isDeleted = false;

  abstract getAllCmd: string;
  abstract getOneCmd: string;
  abstract createCmd: string;
  abstract updateCmd: string;
  abstract deleteCmd: string;

  abstract get modelId(): string;
  abstract typeName: string;

  static createNew<T extends BaseModel>(type: (new () => T)): T {
    const ret: T = new type();
    ret.isNewRecord = true;
    return ret;
  }
  static fromObject2<T>(o: T): T {
    const ret = o as T;
    return o;
  }
  abstract fromObject(o: IBaseModel): IBaseModel;
}

