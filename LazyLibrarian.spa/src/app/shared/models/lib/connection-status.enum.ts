export enum EnumConnectionStatus {
  starting = 0,
  disconnected = 1,
  connecting = 2,
  reconnecting = 3,
  connected = 4
}
