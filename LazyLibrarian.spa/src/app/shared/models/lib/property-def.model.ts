export class PropertyDef {
  public name?: string = undefined;
  public maxLength?: number = undefined;
  public required?: boolean = undefined;
  public toolTip?: string = undefined;
  public label?: string = undefined;
  public isKeyField?: boolean = undefined;
  public propertyKey: string = 'undefined';
}
