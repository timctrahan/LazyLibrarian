import { CUSTOM_ELEMENTS_SCHEMA, Inject, NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// app components
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BooksComponent } from './components/books/books.component';

// app services
import { BookService } from './services';

// third party
import { AgGridModule } from 'ag-grid-angular';
import { FormsModule } from '@angular/forms';
import { LoaderService } from '@shared/services';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AlertModule } from 'ngx-bootstrap/alert';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ToastrModule } from 'ngx-toastr';

// Fonts
import {
  FontAwesomeModule,
  FaIconLibrary,
} from '@fortawesome/angular-fontawesome';
import {
  faPlusCircle, faCheckSquare, faRssSquare, faSpinner, faTrash, faUsersCog, faUserEdit, faEdit,
  faEye, faSearchDollar, faBook, faUser, faPlay, faSearch, faCogs, faBolt, faCalendarPlus, faCalendarAlt, faRss,
  faCircle, faCircleNotch, faEnvelope, faCartArrowDown, faBookOpen

} from '@fortawesome/free-solid-svg-icons';
import { faSquare, faUserCircle } from '@fortawesome/free-regular-svg-icons';
import { BooksGridButtonsComponent } from './components/books/books-grid-buttons.component';

const ICONS = [
  faPlusCircle, faCheckSquare, faRssSquare, faSpinner, faTrash, faUsersCog, faUserEdit, faEdit, faEye, faSearchDollar, faBook, faSquare,
  faUser, faPlay, faSearch, faCogs, faBolt, faCalendarPlus, faCalendarAlt, faRss,
  faCircle, faCircleNotch, faUserCircle, faEnvelope, faCartArrowDown, faBook, faBookOpen
]
const APP_COMPONENTS = [AppComponent, NavbarComponent, BooksComponent, BooksGridButtonsComponent];
const APP_SERVICES = [LoaderService, BookService]
const THIRD_PARTY_MODULES = [
  ToastrModule.forRoot(),
  AgGridModule.withComponents([]),
  ModalModule.forRoot(),
  AlertModule.forRoot(),
  TooltipModule.forRoot(),
  FontAwesomeModule
  // BsDatepickerModule.forRoot(),
  // DigitOnlyModule
];

@NgModule({
  declarations: [
    APP_COMPONENTS
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    THIRD_PARTY_MODULES
  ],
  providers: [
    APP_SERVICES
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {
  constructor(
    @Inject(HTTP_INTERCEPTORS) interceptors: any[],
    private library: FaIconLibrary
  ) {
    library.addIcons(...ICONS)
  }
}
