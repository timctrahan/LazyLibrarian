import { Component } from '@angular/core';
import { LoaderService } from '@shared/services';
import { Book } from './models/Book.model';
import { eBookService } from './services/lib/lazylib.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Lazy Librarian';

  constructor(
    public loaderService: LoaderService) {
  };
}
