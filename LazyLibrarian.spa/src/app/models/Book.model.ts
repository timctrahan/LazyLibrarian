import { BaseModel } from "@shared/models";

export class Book extends BaseModel {
  
  getAllCmd: string = 'getAllBooksMin';
  getOneCmd: string;
  createCmd: string;
  updateCmd: string;
  deleteCmd: string;

  AuthorID!: string;
  AuthorName!: string;
  AuthorLink!: string;
  BookName!: string;
  BookSub!: string;
  BookGenre!: string;
  BookIsbn!: string;
  BookPub!: string;
  BookRate!: number;
  BookImg!: string;
  BookPages!: number;
  BookLink!: string;
  BookID!: string;
  BookDate!: Date;
  BookLang!: string;
  BookAdded!: Date;
  Status!: string;
  typeName = 'book';

  public get modelId(): string {
    return this.BookID;
  }
  public set modelId(value: string) {
    // do nothing
  }

  constructor() {
    super();
  }

  static fromObject(o: any): Book {
    return Object.assign(new Book(), o);
  }

  fromObject(o: Book): Book {
    return Object.assign(new Book(), o);
  }
}

export class BookMin extends BaseModel {
  getAllCmd: string = 'getAllBooksMin';
  getOneCmd: string;
  createCmd: string;
  updateCmd: string;
  deleteCmd: string;

  AuthorID!: string;
  AuthorName!: string;
  BookName!: string;
  BookSub!: string;
  BookRate!: number;
  BookImg!: string;
  BookID!: string;
  BookDate!: Date;
  BookAdded!: Date;
  Status!: string;
  typeName = 'bookmin';

  public get modelId(): string {
    return this.BookID;
  }
  public set modelId(value: string) {
    // do nothing
  }

  constructor() {
    super();
  }

  static fromObject(o: any): Book {
    return Object.assign(new Book(), o);
  }

  fromObject(o: Book): Book {
    return Object.assign(new Book(), o);
  }
}
