import { Observable, throwError, from } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment as env } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Book } from '../../models/Book.model';

@Injectable({
  providedIn: 'root'
})
export  class eBookService {


  constructor(
    /** The class for this service. Should be the same as T */

    protected httpClient: HttpClient) {

    //this._actionUrl = (actionUrl ? actionUrl : `${env.webApiURL}/${this._typeName}`);
  };

  getAll = (forceRefresh = false): Observable<Book[]> => {
    // return this.httpClient.get<any[]>(`http://localhost:5299/get_books?source=Books&booklang=None&sEcho=1&iColumns=9&iDisplayStart=0&iDisplayLength=25&mDataProp_0=0&sSearch_0=&bRegex_0=false&bSearchable_0=true&bSortable_0=false&mDataProp_1=1&sSearch_1=&bRegex_1=false&bSearchable_1=true&bSortable_1=false&mDataProp_2=2&sSearch_2=&bRegex_2=false&bSearchable_2=true&bSortable_2=true&mDataProp_3=3&sSearch_3=&bRegex_3=false&bSearchable_3=true&bSortable_3=true&mDataProp_4=4&sSearch_4=&bRegex_4=false&bSearchable_4=true&bSortable_4=true&mDataProp_5=5&sSearch_5=&bRegex_5=false&bSearchable_5=true&bSortable_5=true&mDataProp_6=6&sSearch_6=&bRegex_6=false&bSearchable_6=true&bSortable_6=true&mDataProp_7=7&sSearch_7=&bRegex_7=false&bSearchable_7=true&bSortable_7=true&mDataProp_8=8&sSearch_8=&bRegex_8=false&bSearchable_8=true&bSortable_8=true&sSearch=&bRegex=false&iSortCol_0=2&sSortDir_0=asc&iSortingCols=1&_=1625075103136`);
    return this.httpClient.get<Book[]>(`http://localhost:5299/api?apikey=a6a7c309b9a792c3bcb3ef58ac067eb5&cmd=getAllBooksMin`);
  }

}
