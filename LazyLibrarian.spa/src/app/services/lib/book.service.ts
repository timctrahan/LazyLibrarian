import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '@app/models/Book.model';
import { AbstractRestService } from '@shared/services';

@Injectable({
  providedIn: 'root'
})
export class BookService extends AbstractRestService<Book> {

  constructor(
    protected httpClient: HttpClient,
  ) {
    super(Book, httpClient);
  }
}
