export const environment = {
  production: true,
  baseURL: location.origin,
  webApiURL: `${location.origin}/api?apikey=a6a7c309b9a792c3bcb3ef58ac067eb5`,
  webURL: `${location.origin}`,
  modelIdSplitChar: '_|_',
  userPermissions: undefined
};
